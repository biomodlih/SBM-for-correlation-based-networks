# Network analysis with stochastic block models (SBM)
These code files are dedicated to perform analyses of correlation-based networks using stochastic block models. It is based on the Python module graph-tool and interfaces R-based script for network generation and analysis. Fitting networks to SBMs delivers communities for the networks as well as edge confidence scores which are a measure of edge relevance. They are closely related to edge existence probabilities and could be used to determine spurious or missing edges.
For the example, metabolite and protein data for breast cancer patients measured by Budczies and colleagues or Mertins and colleagues (CPTAC consortium) has been used. The raw data can be obtained at 
- Budczies J, Brockmöller SF, Müller BM, Barupal DK, Richter-Ehrenstein C, Kleine-Tebbe A, et al. Comparative metabolomics of estrogen receptor positive and estrogen receptor negative breast cancer: alterations in glutamine and beta-alanine metabolism. J Proteomics. 2013;94:279-88.
- Mertins P, Mani DR, Ruggles KV, Gillette MA, Clauser KR, Wang P, et al. Proteogenomics connects somatic mutations to signalling in breast cancer. Nature. 2016;534:55-62.

# Workflow for a network analysis with SBM 

## Prepare networks from data (in R)
Before creating correlation-based biomolecular networks, the molecular entities with too many missing values should be removed. From the measurements, the Hmisc package can be used to establish a correlation matrix along with the p-values (Hmisc::corr() function). These correlation and p-value matrices can be saved and retrieved as R objects using the commands  saveRDS and readRDS, respectively.

### Write edge list with significance of correlation as threshold
Given the symmetric correlation matrix and the p-value matrix of the network as R data files (rds), the R function 'write_edge_list_from_corr_pval()' creates an edge list csv file which can be further used (node indices will start from 0, but the order is kept as in the matrices). Edges with p-value larger than the significance threshold sig_thres are removed, and self-edges are also neglected.
```R
#in R
source(R_analysis_files/create_edge_list.R)
write_edge_list_from_corr_pval('example_files/180502_metabbud_erneg_spear_pval.rds', 'example_files/180502_metabbud_erneg_spear_corr.rds',
  edge_list_filename='180507_metabbud_erneg_spear_BH_005_el.csv',
  multtest_cor_method='BH',
  sig_thres=0.05)
```

### Write edge list with correlation value as threshold
The function 'write_edge_list_from_corr_thres()' can be used to create an edge list csv file usable with graph-tool on the basis of a correlation threshold (i.e. abs(corr) > thres). This procedure is important for reduction by scale-freeness, for which the WGCNA R package can be used to determine a threshold which makes the network as scale-free as possible (with the help of the function RWGCNA::pick_hard_threshold).

```R
#in R
source(R_analysis_files/create_edge_list.R)
write_edge_list_from_corr_thres(load_corr_filename='example_files/180502_metabbud_erneg_spear_corr.rds',
  corr_thres=0.375,
  edge_list_filename='180702_metabbud_erneg_spear_scalefree_el.csv')
```

## Fitting to SBM (in Python)
### Create graph object from edge list or load graph file
The function `read_graph()` allows to create a graph object which can be used with `graph-tool`. It can use csv files as input or graph files and relies on the graph-tool functions `load_graph`
and `load_graph_from_csv`. One additional functionality is that for edge lists, i.e. csv as input, a column named 'edge_weight' will be transformed to have float values. Please avoid naming columns 'edge_index' in the edge list file as this interferes with the according graph-tool edge property and can cause problems when referring to edges. Second, for both reading from graph files as well as csv files, the function can add nodes which are disconnected from any other node (i.e. they would not occur in an edge list file) until the graph has the correct number of nodes (total_node_count). Depends on `numpy`, `graph_tool`.

```Python
#in Python
import read_graph as rg
graph_ini,ew = rg.read_graph('example_files/180905_metabbud_erneg_spear_scalefree_el.csv',total_node_count = 162)
```

### Perform a global fit of the network
The function `global_fit()` is a wrapper to uniformly call the graph-tool functions `minimize_blockmodel_dl()` and `minimize_nested_blockmodel_dl()` with different options for the SBM, hierarchical or not, degree-corrected or not, overlapping or not, weighted or not, and, if weighted, indicating the prior distribution on the weights. Using the default `graph-tool` settings in its simplest form, an additional MCMC sampling for fit improvement can be called. Please note that additional MCMC sampling can take considerable computational resources and not necessary improves the fit result. The function `global_fit()` allows for doing multiple initializations and to return only the best fits (with lowest minimal description length), if desired. Depends on `graph-tool` and `graph_tool.inference` and their dependencies.

```Python
#in Python
# create the graph object graph_ini as described above!
# perform the model fit to a degree-corrected, hierarchical, non-overlapping, non-weighted SBM for 10 initial fits and return the partitions for the 2 best; no additional MCMC fitting
# without additional MCMC fitting, we have 3 return values (best 2 minimal description lengths (not sorted!), partitions for the best 2 fits, minimal description lengths from all 5 initialized fits)
import global_fit as gf
top_mindl0, opt_part0, all_mindl0 = gf.global_fit(graph_ini,random_ini_no = 5,return_fit_no = 2,
model_opt={'dc_SBM':True,'hierarchical_SBM':True, 'overlap_SBM':False, 'weighted_SBM':False},
fit_method_opt={'method_string':'best_state'})

#model fit to non-degree-corrected, non-hierarchical, non-overlapping, weighted SBM (with real-normal prior) for 6 initial fits, performs additional MCMC sampling (with default settings) for the 4 best fits, and returns the partitions for the 2 best fits
#note: with additional MCMC sampling, we have 4 return values - the three from above plus the minimal description lengths from the 4 additional MCMC samplings
top_mindl1, opt_part1, all_mindl1, mcmc_mindl1 = gf.global_fit(graph_ini,random_ini_no = 6,return_fit_no = 2,
model_opt={'dc_SBM':False,'hierarchical_SBM':False, 'overlap_SBM':False, 'weighted_SBM':True,'weight_prior':'real-normal'},
fit_method_opt={'method_string':'with_mcmc','mcmc_fit_no':4})
```

### Writing the results to files which can be used later
Fitting SBM to networks can be time-consuming. Therefore, saving the results for export and/or later usage is important. The function `write_fit_to_file()` is specialized on saving the partitions of multiple optimal SBMs and their minimal description lengths as obtained from `global_fit()` into files which can be used for further analysis also on different platforms.

```Python
#in Python
# create the graph object graph_ini as described above!
# perform a fit to obtain the outputs: top_mindl, opt_part, all_mindl, (mcmc_mindl)
import write_fit_to_file as wf
#first example from above: no additional MCMC sampling, hierarchical SBM
wf.write_fit_to_file(top_mindl0,opt_part0,all_mindl0,mcmc_mindl=None, filename="example_output_file0",blockno_opt={'export_blockno':2,'hierarchical_SBM':True})
#second example from above: additional MCMC sampling, non-hierarchical SBM
wf.write_fit_to_file(top_mindl1,opt_part1,all_mindl1,mcmc_mindl1, filename="example_output_file1",blockno_opt={'export_blockno':2})
```

The block partitions are sorted by optimality, their files are called
"<filename>_optx_runindy_top_block_no.txt" with "x" being 0 for the best fit, 1 for the second best etc., "y" being the index under which the according minimal description length can be found in the first return variable of global_fit(). For hierarchical models, the filenames are called "<filename>_optx_runindy_levz_top_block_no.txt", where "z" is the hierarchy level (integer value between 0 and B) of the partition.

### Example for how to combine the SBM fitting tools to enable batch computations from shell/terminal/commandline
Usually, all three steps, loading the graph, fitting it to SBM and writing it to files, would be performed in a row. The functions described above can be collected into a single Python function to facilitate automatized input, e.g. via calling the function (multiple times) in a bash/shell script. We supply an example of a Python function below which restricts the analysis to degree-corrected, hierarchical SBMs without additional MCMC sampling below (see also file 'fit_dchSBM_noMCM.py'). 

```Python
#in Python
import graph_tool as gt
import global_fit as gf
import write_fit_to_file as wf
import read_graph as rg
import sys

def main():
    el_name =sys.argv[1] #name of edge list to import, given by shell script
    total_node_count=sys.argv[2] #number of total nodes the network should have
    fitno = sys.argv[3] #number of fits to initialize
    out_name = sys.argv[4] #name of the output file, given by shell script in order to write to distinct files
    iterno = sys.argv[5] #use this as addition to the filename to accommodate calling this function (e.g. in a bash loop) multiple times

    #gt.openmp_set_num_threads(1) #use only one core at a time: How many processes are leapt by Python's graph-tool 
    #Option only accessible if openMP was enabled during compilation of graph_tool. 
    #This needs to be especially well controlled for computations in HPC environments. 

    graph_ini,ew = rg.read_graph(el_name,int(total_node_count)) #read edge list and establish network, add nodes to reach target node count

    a1,b1,c1 = gf.global_fit(graph_ini,random_ini_no=int(fitno),return_fit_no=int(fitno), #fitno are fitted and reported
    model_opt={'dc_SBM':True,'hierarchical_SBM':True, 'overlap_SBM':False, 'weighted_SBM':False},
    fit_method_opt={'method_string':'best_state'}) #no MCMC

    wf.write_fit_to_file(a1,b1,c1,None,out_name+iterno,blockno_opt={'export_blockno':int(fitno),'hierarchical_SBM':True}) #export all fitted block partitions


if __name__ == '__main__': #call the script if this calls the function, not if only loaded
    main()
```

Then, assuming that Python and the necessary packages (especially graph-tool and its dependencies) are installed on the system, and the python path contains the directory of where the files global_fit.py etc. are stored, it is sufficient to call this function with the input parameters. The example below shows starting 10 iterations, one after the other, with 5 different initial starting values for the example edge list given in the example folder (attention: many files will be written when runnning this example!). 

```
for i in {1..10} 
do 
python3 fit_dchSBM_noMCMC.py 180905_metabbud_erneg_spear_scalefree_el.csv 162 5 result_file_metab_dchSBM $i 
done 
```

Please note that usually, more than these 50 fits with different initial conditions are required to obtain good (global) fits. Please also note that the results of the fits are only written after all initializations of a run (here: 5) are finished. Please assess runtime and memory consumption on your system before starting many runs.

### Example for execution in HPC environment
In the add-on folder, we provide an example of a shell script (file  'command_fit_dchSBM_noMCMC.sh') which calls the function from above and allows definition and import of other network resources (restricting the run to one node and processor, loading the python and graph-tool modules) to enable applying the function in an HPC environment. Please note that the file needs to be executable by you on your system (this could be accomplished via the command `chmod u+x command_fit_dchSBM_noMCMC.sh`). A possible call of the script for an OAR scheduler is shown below (this likely needs adaptation to your specific HPC environment, please contact your system administrator in case of problems). It would evoke 10 jobs (all named metab_example_dchSBM, time allocation of 12 minutes per job) which are scheduled on the system and, depending on the free resources, can run at the same time. 

```
for i in {1..10}
do 
oarsub -l walltime=00:12:00 -n metab_example_dchSBM -S "./command_fit_dchSBM.sh 180905_metabbud_erneg_spear_scalefree_el.csv 162 5 result_file_metab_dchSBM $i"
done
```

This example is aimed as source of guidance and inspiration and might need heavy adaptations to the employed system, scheduler and other HPC environment parameters. Further levels of external input, e.g. also on the type of the SBM, can easily be accomplished by slight adaptations to this script and to file 'fit_dchSBM_noMCM.py' as explained above. 


## Data analysis of the fits and modules (in R)
We analzed the results of the Python fit further in the R environment. All 

### Retrieve the best fit
First, the fitting results over all iterations (a list of minimal description lengths as saved in files by write_fit_to_file() for each) can be retrieved using the function read_top_mindl(). 
```R
#in R
source(R_analysis_files/read_top_mindl.R)
topmindl_metab_example <- read_top_mindl(filename_start="result_file_metab_dchSBM",
                          filename_end= "",                           containing_folder="example_files/",
                          file_no_list=1:10)
```
The list of vectors with optimal mindls (after all processing) can be used to plot the goodness of fit between different models by calling this script on the results for all different model versions and comparing.

Using this list of vectors, also the block partitions can be retrieved.
```R
#in R
source(R_analysis_files/read_block_levels.R)
all_blocks_metab_example <- read_block_levels(
    filename_start="result_file_metab_dchSBM",
    filename_end= "",
    containing_folder="example_files/",
    file_no_list=1:10,
    is_hier_sbm_=TRUE,
    exportno=5,
    topmindl = topmindl_metab_example,
    max_hier_level=10)
```

### Mapping nodes to their biological entities
Finally, the partitions should be mapped back to the biological entities which gave rise to the correlation matrices and thus edge lists. Note that the networks established via read_graph() do not have the same node order as in the original raw file. Instead, nodes are indexed in the Python graphs in the order of their first appearance in the edge list. This has to be taken into account when retrieving the node partitions and re-assigning the biological names. 

Given the names of the entities in the order how they have been employed to create the correlation-based matrix, we can write the partitions from them, sorted by (global) goodness of fit according to the values given in topmindl_metab_example, into a dataframe using the function `get_partition_biol_anno()`.

```R
#in R
#given the metabolite names in the order used to establish the correlation files
metab_biol_entities <- read.table("example_files/Budczies_metabolite_names.txt",sep="\t", stringsAsFactors = FALSE)
source(R_analysis_files/get_partition_biol_anno.R)
all_part_biol_metab_example <- get_partition_biol_anno(topmindl_list=topmindl_metab_example,
  block_list=all_blocks_metab_example,
  biol_entities=metab_biol_entities,
  edge_list=read.table('example_files/180905_metabbud_erneg_spear_scalefree_el.csv',sep=",",header=T),
  is_hierarchical = TRUE,
  max_hier_level = 'all')
```

For a single (hierarchical) partition, only the mapping is performed.
```R
#in R
singlepart_biol_metab <- get_partition_biol_anno(topmindl_list=list(data.frame(V1=1)), 
    block_list=all_blocks_metab_example[[1]], 
    biol_entities=metab_biol_entities, 
    edge_list=read.table('180905_metabbud_erneg_spear_scalefree_el.csv',sep=",",header=T),
    is_hierarchical = TRUE,
    max_hier_level = 'all')
```

### Enrichment analyis of mRNA, protein and metabolite data
The enrichment of biological function in predicted modules can be assessed by using different sources of biological function annotation. We used Reactome Pathways (for mRNA, protein expression networks) or KEGG Pathways (for metabolites). Alternative sources could be EBI GO terms, PANTHER Pathways, or KEGG modules. For Reactome Pathway enrichment, we used the R package ReactomePA, for the metabolites the R package clusterProfiler for a user-defined annotation file, which we derived manually, together with the KEGG compound metabolite identifiers, from the MBRole webserver (version 2).

For the metabolites, we need to map the metabolite names first to KEGG compound IDs (saved to a file beforehand).
```R
#in R
  #import partition
  metabbud_sf_bestpart <- readRDS("example_files/180912_metabbud_erneg_sf_bestpart.RDS")
  #retrieve metabolite identifier (KEGG compound IDs)
  metab_names <- read.table("enrichment_analysis/Budczies_metabolite_biological_identfier_edgelist.tsv",
  sep="\t",header=T,stringsAsFactors = F,quote="")
  #retrieve KEGG pathway ontology for the KEGG compound IDs from the metabolites in the file
  KEGGPW_2_KEGGID <- read.table("enrichment_analysis/KEGGpathwayAnno2KEGGID_metaboliteBudczies2013.txt",
  sep="\t")
  #retrieve the names of the KEGG Pathway IDs
  KEGGPW_2_NAME <- read.table("enrichment_analysis/KEGGpathwayAnno2NAME_metaboliteBudczies2013.txt",sep="\t")
  source(R_analysis_files/enrichment_partitions_OwnAnno.R)
  #apply the function to the lowest level partition
  metab_part_enrichment <- compute_enrichOwnAnno_partition(
                          part_table=cbind(metab_names$KEGG_ID,metabbud_sf_bestpart[,2]),
                          background_genes=unique(KEGGPW_2_KEGGID$KEGGID),
                          term_2_gene=KEGGPW_2_KEGGID,
                          term_2_name=KEGGPW_2_NAME)
```

For mRNA or protein data and every biological entity whose identifiers can be easily mapped to ENTREZID, the enrichment of Reactome Pathways using the following function is appropriate. Please note that the example partition file here contains SYMBOL as identifier.
```R
#in R
#import a partition file
  protein_example_part <- readRDS("example_files/180912_protulr_erneg_sf_bestpart.RDS")
#find background genes
  background_genes_ENTREZ <- clusterProfiler::bitr(protein_example_part[,1], fromType = "SYMBOL", toType = "ENTREZID",OrgDb="org.Hs.eg.db")$ENTREZID
#compute enrichment
source(R_analysis_files/enrichment_partitions_Reactome.R)
  protein_enrichment <- compute_enrichPathway_partition(protein_example_part[,1:2],
    background_genes=background_genes_ENTREZ,
    keytype="SYMBOL")
```

The output of these functions are a list of the results for each partition, and summary statistics of the enrichment for each block in a dataframe.

### Assessing level of enrichment by chance
Enrichment of modules in biological function might occur by chance. Therefore, comparing it to a random partition with the same properties is key to assess the relevance of the detected enrichment levels. Using the same functions as for the enrichment analysis, this can be done easily by shuffling the annotations of the genes (in the line of argumentation to permutation tests). The characteristics of the partitions thus remain identical.

```R
#in R
#using exactly the same objects as in the enrichment analysis above
#randomly shuffle the metabolite identifiers
random_shuffle_metab_erneg_sf <- sample(metabbud_sf_bestpart_KEGGC, size=162, replace = FALSE, prob = NULL)
#perform enrichment on shuffled metabolite identities, level 1 partition
random_enrichment_example <- compute_enrichOwnAnno_partition(
    cbind(random_shuffle_metab_erneg_sf,metabbud_sf_bestpart[,2]), 
    background_genes = unique(KEGGPW_2_KEGGID$KEGGID), 
    term_2_gene=KEGGPW_2_KEGGID)
#number of blocks with a functional annotation in KEGG pathways (term_count>0)
sum(random_enrichment_example[[2]]$term_count>0)
```
In order to obtain exact assessments of how far from random the number of blocks with functional enrichment is (i.e. assigning a p-value), we would have to do many of these random permutations. Due to the high computational effort for enrichment computations for partitions with many blocks (and/or ontologies with many pathways), we resorted to only assessing it few times to obtain an overall impression.

### Distance computations between Reactome pathways and within hierarchical SBMs (in R)
Functions for distance measures within Reactome hierarchies or the SBM are saved in R_analysis_file/distances_in_hierarchies.R

## Edge confidence score calculation (in Python)
Edge confidence score computation relies on the graph-tool function get_edges_prob(). Please note that at the time of writing, there is an issue with this function for weighted SBMs with real-normal edge covariate (see issue #452 on the graph-tool git, git.skewed), and using it for this case is not recommended until this issue has been resolved. 
The function compute_single_edge_prob() offers the computation of single edge confidence scores, either for all missing and spurious edges of the underlying graph of the SBM, or for given lists of edges. The functions in read_SBM.py are convenience wrapper for SBM initialization and rely on read_graph.py; they offer an interface to retrieve SBMs from a given edge list and a partition as written by write_fit_to_file().

### Reading in the SBMs
Before computing edge scores, the SBMs need to be defined. This can happen via the following command to retrieve content from files, or the SBM can be directly used after fitting within Python.

```Python
#in Python
import read_SBM as rsbm
#only read in a partition (as written by write_fit_to_file()), this can be used manually as argument `b` or `bs` together with BlockState or NestedBlockState from graph_tool
partition = rsbm.read_block_partition("example_files/metabbud_erneg_sf_hSBM_bestfit_block_lev", ".txt",no_hier_levels = 3)

#establish an SBM from a given edge list and partition file name, this is a convenience wrapper only to call read_graph() and the graph-tool initialization functions
sbm_metab = rsbm.read_SBM("example_files/180905_metabbud_erneg_spear_scalefree_el.csv",
162,
"example_files/metabbud_erneg_sf_hSBM_bestfit_block_lev",
".txt",
no_hier_levels = 3,
is_hierarchical=True,
is_degree_corrected=False)
```

### Computing edge confidence scores for all single edges
The function compute_single_edge_prob() provides the option for edge computation and relies on parallelized computing via the Python module 'multiprocessing'. Also, the Python module 'functools' is required. The function will give back a list of three elements: edge scores for spurious edges, edge score for missing edges between nodes which have at least one existing edge, edge score for missing edges between a node without any edge and the other nodes. The output contains the source and target node indices of the edge along with its score.
```Python
#in Python
import calc_edge_prob as calcedge
edgeprobs_mult = calcedge.compute_single_edge_prob(sbm_metab, 
    compute_all_spurious=True, 
    compute_all_missing=True, 
    disconn_nodes=[161],multiprocess_nodecount=4,
    maxedges_perjob=1000)
#save as numpy arrays (this format takes less space than text files, but needs to be accessed via Python)
import numpy as np
np.save(file="metab_example"+"_spur_edge_prob.npy", arr=edgeprobs_mult[0]) 
np.save(file="metab_example"+"_miss_connedge_prob.npy", arr=edgeprobs_mult[1]) 
np.save(file="metab_example"+"_miss_disconnedge_prob.npy", arr=edgeprobs_mult[2]) 
```

### Computing edge confidence score via a command line
A convenience function for shell input is given in file 'compute_all_edgeprobs.py' which writes the edge score into three txt files. 

```Python
import graph_tool as gt 
import calc_edge_prob as calcedge
import read_SBM as rsbm
import numpy as np
import sys


def main():
    el_name = sys.argv[1] #name of edge list to import
    total_node_count = sys.argv[2] #add nodes such that the graph has that number of nodes
    start_blockfile = sys.argv[3] #start of filename of partition file of SBM to use
    end_blockfile = sys.argv[4] #end of filename of partition file
    level_count = sys.argv[5] #number of hierarchy levels (i.e. count of files)
    is_hier = sys.argv[6] #if it is hierarchical (1) or not (0)
    is_dc = sys.argv[7] #if it is degree corrected (1) or not (0)
    conn_node_count = sys.argv[8] #number of connected nodes (i.e. those appearing in the edge list) 
    out_name = sys.argv[9] #name of the output files, given by shell script in order to write to distinct files
    compnode_count = sys.argv[10] #how many jobs are allowed to run in parallel
    edgecount_perjob = sys.argv[11] #how many edges should be computed per job


    #gt.openmp_set_num_threads(1) #uncomment this line to avoid uncontrolled spawning in HPC environments, not available if graph-tool was compiled without OpenMP option

    #initialize sbm
    sbm = rsbm.read_SBM(el_name,int(total_node_count), 
        start_blockfile,end_blockfile,
        no_hier_levels = int(level_count),
        is_hierarchical=bool(int(is_hier)),
        is_degree_corrected=bool(int(is_dc)))
    
    #compute edge probabilities
    edgeprobs = calcedge.compute_single_edge_prob(sbm, 
        compute_all_spurious=True, 
        compute_all_missing=True, 
        disconn_nodes=range(int(conn_node_count),int(total_node_count)),
        multiprocess_nodecount=int(compnode_count),maxedges_perjob=int(edgecount_perjob))

    #write to txt file, tab-separated
    with open(out_name + "_spur_edge_prob.txt", 'w') as thefile:
        for item in edgeprobs[0]:
            thefile.write("%s\t%s\t%s\n" % (item[0],item[1],item[2])) #spurious edges

    with open(out_name + "_miss_connedge_prob.txt", 'w') as thefile:
        for item in edgeprobs[1]:
            thefile.write("%s\t%s\t%s\n" % (item[0],item[1],item[2])) #missing edges between two nodes of degree>0

    with open(out_name + "_miss_disconnedge_prob.txt", 'w') as thefile:
        for item in edgeprobs[2]:
            thefile.write("%s\t%s\t%s\n" % (item[0],item[1],item[2])) #missing edges for a node of degree=0
    
if __name__ == '__main__': #call the script if this calls the function, not if only loaded
    main()
```

This function can then be called via command line, using in this case 4 parallel jobs with 500 edge probability calculations per job. Note that this is way too much for that simple system and only serves for illustration; the overhead for starting many jobs will be larger than the benefit from multiprocessing.
```
python3 compute_all_edgeprobs.py example_files/180905_metabbud_erneg_spear_scalefree_el.csv 162 example_files/metabbud_erneg_sf_hSBM_bestfit_block_lev .txt 3 1 0 161 metab_example 4 500
```


### Generating missing edge lists for which to compute confidence scores (in R)
The function determine_missing_edge_lists() in R can be used to generate a list of all missing edges of a network just from the edge list and the number of nodes, or lists of edges ranked by a certain value (e.g. correlation, p-value) up to a threshold or up to a certain number of edges. The output will be (a list of) two lists of edges: the first list contains only nodes which occur in the edge list, the second list contains edges which connect at least one completely disconnected node with another. Its last element will be the edge between two completeley disconnected nodes. 

Compiling lists of all missing edges is the default.
```R
#in R
el_metab <- read.table('example_files/180905_metabbud_erneg_spear_BH_005_el.csv',
         sep=',',header=TRUE)
total_node_count <- 162
source(R_analysis_files/determine_missing_edge_lists.R)
missing_edge_lists <- determine_missing_edge_lists(edge_list=el_metab,total_node_count=total_node_count)
```

For the example with edges ranked by values above a certain threshold, more information is required for all edges, given in a symmetric square matrix. Here, these are correlation values. Attention: If delivering a matrix of p-values, the significances need to be inverted (-p_val) in order to pick edges that are most significant.
```R
el_metab <- read.table('example_files/180905_metabbud_erneg_spear_BH_005_el.csv',
           sep=',',header=TRUE)
total_node_count <- 162
corr_metab <- readRDS('example_files/180502_metabbud_erneg_spear_corr.rds')
source(R_analysis_files/determine_missing_edge_lists.R)
missing_edge_lists_threshold <- determine_missing_edge_lists(edge_list=el_metab,
                                     total_node_count=total_node_count,
                                     threshold_values=abs(corr_metab),
                                     method_conn='threshold',
                                     method_disconn='threshold',
                                     cut_off_conn=0.3,
                                     cut_off_disconn=0.1)

```

Similar information can be used to return only a certain number of edges, ranked by the values given in threshold_values. This example should deliver the same output as the example above.
```R
missing_edge_lists_threshold_count  <- determine_missing_edge_lists(edge_list=el_metab,
                                    total_node_count=total_node_count,
                                    threshold_values=abs(corr_metab),
                                    method_conn='edge_count',
                                    method_disconn='edge_count',
                                    edge_count_conn=194,
                                    edge_count_disconn=66)
```



#### Read in an edge list created in R for use with compute_single_edge_prob (in Python)
An edge list file, as e.g. created using the R function provided above, should contain one line for each edge, in which the indices of the corresponding nodes (integer values) are tab-separated. The following function brings this edge list into a format which is required for edge confidence score computation (assumed the correct indices for the nodes as employed in the Python graph).
```Python
#in Python
import calc_edge_prob as calcedge
miss_edge_list = calcedge.read_in_edge_list_from_file('metab_example_missing_edge_list_connected_nodes.txt')
```


### Computing edge confidence scores using lists of edges (in Python)
The same function as above can be used to compute edge confidence scores only for edges given in a list. These lists need to be actual edge instances from the SBM (for spurious edge lists) or lists of tuples, for which each tuple is composed from the node indices for the edge, for the missing edge lists. 

```Python
#in Python
#Assume that sbm_metab is the SBM generated from the example files and used before.
import calc_edge_prob as calc_edge
#Read in the edge lists (as generate from the R code above).
miss_edge_list_conn = calcedge.read_in_edge_list_from_file("example_files/metab_example_missing_edge_list_connected_nodes.txt")
  miss_edge_list_disconn = calcedge.read_in_edge_list_from_file("example_files/metab_example_missing_edge_list_disconnected_nodes.txt")
  edgeprobs = calcedge.compute_single_edge_prob(sbm_metab, 
    compute_all_spurious=False, 
    compute_all_missing=False, 
    disconn_nodes=[161],
    miss_edge_list=[miss_edge_list_conn,miss_edge_list_disconn],
    multiprocess_nodecount=4,maxedges_perjob=100)
```

The script 'compute_all_edgeprobs.py' from above can easily be adapted to accommodate input of edge lists for which to compute edge confidence scores, see the file 'compute_missing_edgeprobs_from_file.py' in the add-on folder for an example and for how to write the results to files.



### Mapping edge indices from Python files to the initial graph indices (in R)
If the graphs were established using edge lists, the node indices might not coincide between the Python graph-tool object and the initial network. Therefore, the R function 'convert_id_Python2edgelist()'' provides a mapping between the indices. Note that the edge lists are designed to start from index 0 onwards. Thus, to gather the indices of the correlation files in R, addition by 1 might be necessary.

```R
#in R
source("R_analysis_files/convert_id_Python2edgelist.R")
edgeproblist <- read.table("example_files/metab_example_miss_connedge_prob.txt", sep="\t")
colnames(edgeproblist) <- c("source_node","target_node","edge_prob")
orig_ID_sources <- convert_id_Python2edgelist(edgeproblist$source_node,"example_files/180905_metabbud_erneg_spear_scalefree.csv",161)
```

## Alternative clustering using WGCNA R package
For clustering the correlation-based networks with the technique from the WGCNA R package, we use the complete weighted networks and the approach from the step-by-step network construction and module detection WGCNA tutorial by Horvath and Langenfelder (https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/FemaleLiver-02-networkConstr-man.R). First, the soft threshold is determined. This is the exponent by which the correlation values are to be raised in order to enforce a scale-free architecture of the weighted network.

```R
#read in the correlation file 
similarity= readRDS("example_files/180502_metabbud_erneg_spear_corr.rds")

#load the WGCNA library
library(WGCNA)
#transform to values between 0 and 1
adjacency_mat <- adjacency.fromSimilarity(similarity, 
                         type = "signed", 
                         power = 1)

#assess which exponent to use
softthres <- pickSoftThreshold.fromSimilarity(
  adj_mat_metab, RsquaredCut = 0.85, powerVector = c(seq(1, 10, by = 1), seq(12, 20, by = 2)), removeFirst = FALSE, nBreaks = 10, blockSize = 1000,
  moreNetworkConcepts=FALSE, verbose = 0, indent = 0)
```

Then, the dissimilarity matrix is determined, a hierarchical clustering is performed and the clusters are determined by cutting the hierarchical clustering tree.

```R
#dissimilarity matrix
TOM = TOMsimilarity(adjacency_mat^softthres$powerEstimate)
dissTOM = 1-TOM

#hierarchical cluster tree
geneTree = hclust(as.dist(dissTOM), method = "average")

#cutting the tree gives the cluster labels for each node (in order of correlation matrix)
dynamicMods = cutreeDynamic(dendro = geneTree, distM = dissTOM,
                            deepSplit = 2, pamRespectsDendro = FALSE,
                            minClusterSize = 3)
```

The computed clustering for the metabolite data (using KEGG compound identifier) is given in example_files/190212_metab_erneg_spear_WGCNA_clustering_expo12_min3_split2.tsv.
















