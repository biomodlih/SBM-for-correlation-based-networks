#!/bin/bash -l
#OAR -l nodes=1/core=1

module use /opt/easybuild/modules/all
module load Python/3.6.3-foss-2017b
module load graph-tool


python fit_dchSBM_noMCMC.py $1 $2 $3 $4 $5
