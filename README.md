# SBM-for-correlation-based-networks

This repository contains both Python-based and R-based code files for analysis of correlation-based networks using their representation as stochastic block models (SBM). Clusters/modules can be determined as well as edge confidence scores which indicate edge relevance. Fitting to SBM relies on the Python module graph-tool (by Tiago Peixoto, https://graph-tool.skewed.de). Examples shown here are covering biomolecular networks.
    
A possible workflow is described in detail in the file *Workflow_network_analysis_with_SBM.md*. The folders *example_files* and *enrichment_analysis* contain files necessary to run the workflow examples.

The Python modules are 
- *calc_edge_prob*
- *global_fit*
- *read_graph*
- *read_SBM*
- *write_fit_to_file*

They are documented separately in html files in the folder *Python_documentation*.

The files *compute_all_edgeprobs.py* and *fit_dchSBM_noMCMC.py*
showcase how to bring the different modules and functions together for edge confidence score computation or fit to SBM, respectively.

The folder *R_analysis_files* contains the collection of source code of the employed R functions.

The folder *add-on* contains other useful, but not required files which can be of interest if using a high performance computing environment.

All code is licenced under the GNU GPLv3, SBM-for-correlation-based-networks, Copyright (C) 2019  Katharina Baum.





