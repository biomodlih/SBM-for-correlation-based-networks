

def write_fit_to_file(topmindl, topblockno, mindl, mcmc_mindl,filename,blockno_opt):
	""" 
	Writes the data from global_fit into files. 

	Attention: Existing files of the same names are overwritten without warning. 
	The files will end by

		- '_top_mindl.txt' for the best fits 
		- '_mindl.txt' for all initial fits
		- '_mcmc_mindl.txt' for the fits after additional MCMC sampling, 
		- '_top_block_no.txt' for the best partitions, number given in blockno_opt

	The partition filename string will contain 'optk' for the kth best fit supplied to the function,
	it will contain 'runindk' with k indicating the position of this fit in the _top_mindl.txt file,
	it will contain 'levk' with k indicating the hierarchy level of the partition. For example, lev0 marks the partition of
	the nodes from the initial graph into blocks, lev1 marks the partition of the first-level blocks into blocks etc.
	The last level cumulates in the trivial partition of one top block.
	
	:Parameters:
	
	- topmindl: best minimal description lengths as returned as first variable from global_fit()
	- topblockno: block partitions, in the same order as topmindl, as returned from global_fit()
	- mindl: all ever computed minimal description lengths, returned as third variable from global_fit()
	- mcmc_mindl: (optional) all minimal description lengths after additional MCMC sampling (4th output from global_fit())
	- filename: how the written files should be named (except for their conserved ending), without extension
	- blockno_opt: 

		- 'export_blockno': integer [default: 5] - how many block states should be saved as files, 
		  all will be written if integer is larger than number of provided partitions
		- 'hierarchical_SBM': Boolean [default: False] - if non-nested SBM (False) or nested SBM (True) was used
	
	:Returns:
	
	No return value.
	
	:Examples:
	
	For a fit without MCMC sampling, hierarchical SBM::
	
		import write_fit_to_file as wf
		wf.write_fit_to_file(topmindl=top_mindl0,
			topblockno=opt_part,
			mindl=all_mindl,
			mcmc_mindl=None, 
			filename="example_output_file0",
			blockno_opt={'export_blockno':2,'hierarchical_SBM':True})
	
	For a fit with additional MCMC sampling, non-hierarchical SBM (default option)::
	
		wf.write_fit_to_file(top_mindl1,
		opt_part1,
		all_mindl1,
		mcmc_mindl1, 
		filename="example_output_file1",
		blockno_opt={'export_blockno':2})
	
	"""

	if blockno_opt==None:
		blockno_opt={'export_blockno':len(topblockno),'hierarchical_SBM':False}
	else:
		if not 'export_blockno' in blockno_opt:
			blockno_opt['export_blockno']=len(topblockno)
		if not 'hierarchical_SBM' in blockno_opt:
			blockno_opt['hierarchical_SBM']=False

	with open(filename+'_top_mindl.txt','w') as thefile:
		for item in topmindl:
			thefile.write('%.4f\n' % item)

	with open(filename+'_mindl.txt','w') as thefile:
		for item in mindl:
			thefile.write('%.4f\n' % item)
	
	if not mcmc_mindl==None:
		with open(filename+'_mcmc_mindl.txt','w') as thefile:
			for item in mcmc_mindl:
				thefile.write('%.4f\n' % item)


	#save only the blockno_opt: 'export_blockno' best partitions
	sort_res=sorted(topmindl)
	for i in range(min(len(topblockno),blockno_opt['export_blockno'])):
		runind=topmindl.index(sort_res[i])
		if blockno_opt['hierarchical_SBM']:
			for j in range(len(topblockno[runind])):
				with open(filename+ '_opt'+str(i)+'_runind'+str(runind)+'_lev'+str(j)+'_top_block_no.txt','w') as thefile:
					for item in topblockno[runind][j]:
						thefile.write('%i\n' % item)
		else:
			with open(filename+ '_opt'+str(i)+'_runind'+str(runind)+'_top_block_no.txt','w') as thefile:
				for item in topblockno[runind]:
					thefile.write('%i\n' % item)

