
import graph_tool as gt # only for setting one thread at a time
import calc_edge_prob as calcedge
import read_SBM as rsbm
import numpy as np
import sys


def main():
	el_name = sys.argv[1] #name of edge list to import, given by shell script
	total_node_count = sys.argv[2] #add nodes such that the graph has that number od nodes
	start_blockfile = sys.argv[3] #start of filename of partition file of SBM to use
	end_blockfile = sys.argv[4] #end of filename of partition file
	level_count = sys.argv[5] #number of hierarchy levels (i.e. count of files)
	is_hier = sys.argv[6] #if it is hierarchical (1) or not (0)
	is_dc = sys.argv[7] #if it is degree corrected (1) or not (0)
	conn_node_count = sys.argv[8] #number of connected nodes (i.e. those appearing in the edge list) 
	out_name = sys.argv[9] #name of the output files, given by shell script in order to write to distinct files
	compnode_count = sys.argv[10] #how many jobs are allowed to run in parallel
	edgecount_perjob = sys.argv[11] #how many edges should be computed per job


	#gt.openmp_set_num_threads(1) #uncomment this line to avoid uncontrolled spawning in HPC environments, 
	#not available/necessary if graph-tool was compiled without OpenMP option

	#initialize sbm
	sbm = rsbm.read_SBM(el_name,int(total_node_count), 
		start_blockfile,end_blockfile,
		no_hier_levels = int(level_count),
		is_hierarchical=bool(int(is_hier)),
		is_degree_corrected=bool(int(is_dc)))
	
	#compute edge probabilities
	edgeprobs = calcedge.compute_single_edge_prob(sbm, 
		compute_all_spurious=True, 
		compute_all_missing=True, 
		disconn_nodes=range(int(conn_node_count),int(total_node_count)),
		multiprocess_nodecount=int(compnode_count),maxedges_perjob=int(edgecount_perjob))

	#write to txt file, tab-separated
	with open(out_name + "_spur_edge_prob.txt", 'w') as thefile:
		for item in edgeprobs[0]:
			thefile.write("%s\t%s\t%s\n" % (item[0],item[1],item[2])) #spurious edges

	with open(out_name + "_miss_connedge_prob.txt", 'w') as thefile:
		for item in edgeprobs[1]:
			thefile.write("%s\t%s\t%s\n" % (item[0],item[1],item[2])) #missing edges between two nodes of degree>0

	with open(out_name + "_miss_disconnedge_prob.txt", 'w') as thefile:
		for item in edgeprobs[2]:
			thefile.write("%s\t%s\t%s\n" % (item[0],item[1],item[2])) #missing edges for a node of degree=0
	
if __name__ == '__main__': #call the script if this calls the function, not if only loaded
	main()
